#!/bin/bash

# Update package list
sudo apt-get update

# Install dependencies for Git, Tmux, Docker, and Pyenv (excluding python-openssl)
sudo apt-get install -y git tmux apt-transport-https ca-certificates curl software-properties-common \
                        build-essential libssl-dev zlib1g-dev libbz2-dev \
                        libreadline-dev libsqlite3-dev wget llvm libncurses5-dev libncursesw5-dev \
                        xz-utils tk-dev libffi-dev liblzma-dev

# Clone and install Oh My Bash
git clone https://github.com/ohmybash/oh-my-bash.git ~/.oh-my-bash
bash ~/.oh-my-bash/tools/install.sh

# Install Tmux
sudo apt-get install -y tmux

# Install Docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io

# Add current user to Docker group to avoid permission issues
sudo usermod -aG docker $USER

# Install Docker Compose
sudo curl -L "https://github.com/docker/compose/releases/download/v2.2.3/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# Install Pyenv
git clone https://github.com/pyenv/pyenv.git ~/.pyenv
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n  eval "$(pyenv init -)"\nfi' >> ~/.bashrc
source ~/.bashrc

# Install Python 3.12 using Pyenv, if not already installed
if ! pyenv versions | grep -q '3.12'; then
    pyenv install 3.12
    pyenv global 3.12
fi

# Install Python libraries from defaults.txt
pip install -r defaults.txt

# Install VSCode
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
sudo mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
sudo apt-get update
sudo apt-get install -y code

# Install Python extension and Material Icon Theme for VSCode
code --install-extension ms-python.python
code --install-extension PKief.material-icon-theme

echo "Installation complete. Please restart your terminal."
