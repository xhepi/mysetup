# Development Environment Setup Script

This repository contains a Bash script (`setup.sh`) for setting up a basic development environment on Pop!_OS or Ubuntu-based systems. The script installs several essential tools and configurations for a smooth development experience. Additionally, there is a `defaults.txt` file that lists common Python libraries for quick installation.

## What's Inside

### setup.sh

The `setup.sh` script automates the installation of the following components:

- **Oh My Bash**: An enhanced shell experience for Bash users.
- **Tmux**: A terminal multiplexer.
- **Docker & Docker Compose**: Essential tools for containerization.
- **Pyenv**: A tool for managing multiple Python versions.
- **Python 3.12**: The latest Python version (as of the script's last update).
- **Visual Studio Code (VSCode)**: A popular code editor.
- **VSCode Extensions**: Python extension, Dracula Theme, and Material Icon Theme.

### defaults.txt

The `defaults.txt` file includes a list of commonly used Python libraries, such as:

- Data processing and scientific computing libraries (`numpy`, `pandas`, etc.).
- Web development libraries (`flask`, `django`).
- Machine Learning libraries (`tensorflow`, `torch`, `keras`).

These libraries can be installed using `pip` to quickly set up a Python development environment.

## Usage

1. Clone this repository to your local machine.
2. Navigate to the cloned directory.
3. Make the script executable: `chmod +x setup.sh`.
4. Run the script: `./setup.sh`.
5. Python libraries can be installed using: `pip install -r defaults.txt`.

## Prerequisites

- This script is intended for use on Pop!_OS or Ubuntu-based distributions.
- Ensure you have administrative (sudo) privileges on your system to install the necessary packages.

## Customization

You can modify `setup.sh` and `defaults.txt` to include additional tools or libraries as per your requirement.
